// 

/*********************** VARIABLE AND DATA TYPES  **************************/
// NUMBER
// STRING
// BOOLEAN
// Undefined
// NULL
// var firstName = "John";
// console.log(firstName);

// var lastName = "Doe";
// console.log(lastName);

// var age = 22;
// console.log(age);

// var fullAge = true;
// console.log(fullAge);

// var job;
// console.log(job);

// var job = "Web Developer";
// console.log(job);

// var nothing = null;
// console.log(nothing);



/*********************** VARIABLE MUTATION AND TYPE COERCION **************************/

// var firstName = 'John';
// var age = 22;

// // TYPE COERCION
// console.log(firstName + ' ' + age);

// var job,isMarried;
// job = 'Web Developer';
// isMarried = false;

// console.log(firstName + ' is a ' + age + '  year old ' + job + ' . Is he married ? ' + isMarried);

// // VARIABLE  MUTATION
// age = 'twenty two';     // MUTATED FROM A NUMBER TO A STRING. MAGIC OF JAVASCRIPT DYNAMIO TYPING
// job = 'Teacher';

// alert(firstName + ' is a ' + age + '  year old ' + job + ' . Is he married ? ' + isMarried);

// var lastName = prompt('What is his last name ?');
// console.log(firstName + ' ' + lastName);

/*********************** BASIC OPERATORS **************************/

// var currentYear = 2019;

// var yearJohn = currentYear -  22;
// console.log("Year of birth for John is: " + yearJohn);

// var yearMark = currentYear - 24;
// console.log("Year of birth for Mark is: " + yearMark);

// console.log("Adding 5 years: " + (yearMark + 5));
// console.log("Times 6 years: " + (5 * 6));

// typeof operator tells what type of operator it is 
// var name = 'John';
// var value = true;
// var nothing = null;
// var x;


// console.log("Type of yearMark : "+typeof(yearMark));
// console.log("Type of name : "+typeof(name));
// console.log("Type of value : "+typeof(value));
// console.log("Type of nothing : "+typeof(nothing));
// console.log("Type of x : "+typeof(x));


/*********************** OPERATORS PRECENDENCE **************************/

// var now = 2019;
// var yearJohn = 1997;
// var fullAge = 22;

// var isAdult = now - yearJohn >= fullAge;

// console.log(isAdult);

// var ageJohn = now - yearJohn;
// var ageMark = 35;
// var avg = (ageJohn + ageMark) / 2;
// console.log(avg);

// // Multiple Assignments

// var y = (34 + 54) - 12 * 4 / 4;    
// console.log(y);

/*********************** CODING CHALLENGE 1 **************************/


// Mass is kg
// var johnMass = 63;
// var markMass = 65;

// // Height in meters
// var johnHeight = 5.2;
// var markHeight = 5.0;

// var johnBMI = johnMass / (johnHeight * johnHeight);
// var markBMI = markMass / (markHeight * markHeight);
// console.log(johnBMI,markBMI);

// var isBMIHigher = markBMI > johnBMI;

// console.log("Is Mark's BMI higher than John's? " + isBMIHigher);


/*********************** IF / ELSE STATMENT **************************/

// var firstName = 'John';
// var civilStatus = 'single';

// if(civilStatus === 'married'){
//     console.log(firstName + ' is married.');
// } else {
//     console.log(firstName + ' will hopefully marry soon.');
// }

// var isMarried = true;

// if(isMarried){
//     console.log(firstName + ' is married.');
// } else {
//     console.log(firstName + ' will hopefully marry soon.');
// }

/*********************** BOOLEAN LOGIC **************************/

// var firstName  = 'John';
// var age = 19;

// if(age < 13){
//     console.log(firstName + " is a boy.");
// } else if(age >= 13 && age < 20){
//     console.log(firstName + " is a teenager.");
// } else if(age >= 20 && age < 30){
//     console.log(firstName + " is a young man.");
// }else {
//     console.log(firstName + " is a man.");
// }











